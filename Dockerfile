ARG PYTHON_IMAGE_TAG=3.8.7
FROM python:${PYTHON_IMAGE_TAG}
MAINTAINER SatNOGS project <dev@satnogs.org>

ARG SATNOGS_UID=999

# Create 'satnogs' user and group
RUN groupadd -r -g ${SATNOGS_UID} satnogs \
	&& useradd -r -g satnogs -u ${SATNOGS_UID} -s /sbin/nologin -d /nonexistent satnogs

# Copy source code
COPY . /usr/local/src/satnogs-router/

# Install 'satnogs-router'
RUN pip install \
	--no-cache-dir \
	--no-deps \
	--force-reinstall \
	-r /usr/local/src/satnogs-router/requirements.txt \
	/usr/local/src/satnogs-router \
	&& rm -rf /usr/local/src/satnogs-router

# Expose 'satnogs-router' ports
EXPOSE 5555
EXPOSE 5560

# Run application
USER satnogs
CMD ["satnogs-router"]
